import numpy as np
import matplotlib.pyplot as plt

rewa = []

goal = "target"

class envi():
    '''class envi:the environment
    env_size 11*10
    action_size 3
    '''
    def __init__(self) -> None:
        self.h = 4
        self.l = 4
        self.env_size = self.h * self.l + 2
        self.action_size = 4
        
        self.map = np.zeros((self.h + 1, self.l))
        self.map[1,1] = -1
        self.obs = 1
        #self.map[4,4] = -1
        #self.position = [0,0]
        self.map[0,0] = 1
        self.step_c = 0
       
        
        
        
        
        

    def reset(self):
        '''[summary] reset the maze 
        Returns:
            [self.map]: 11*10 matrix
        '''
        self.__init__()
        
        #return self.map
        return self.map
    
    def step(self, action):
        '''[summary] 

        Args:
            action (one of [0,1,2,3]): [0 is up;1 is down;2 is left; 3 is right]

        Returns:
            [s_, reward, done, self.position]: [description]
        '''
        s = self.map
        self.position = self.map[self.h , 0:2].astype(int)
        self.step_c += 1
        cross = False
        if(action == 0):  #up
            if(self.position[0] >= 1):
                print("0 up")
                self.position[0] -= 1
            else:
                cross = True

        if(action == 1):  #down
            if(self.position[0] <=2):
                print("1 down")
                self.position[0] += 1  
            else:
                cross = True
        
        if(action == 2):  #left
            if(self.position[1] >= 1):
                print("2 left")
                self.position[1] -= 1
            else:
                cross = True
           
        if(action == 3):  #right
            if(self.position[1] <= 2):
                print("3 right")
                self.position[1] += 1
            else:
                cross = True
        
        if goal == "ccpp":
        #hit the obstacle
        
            if(self.map[self.position[0],self.position[1]] == -1 or cross):
                reward = -1
                done = True
                print("done")
            # elif(np.sum(self.map) >= 400):
            #     reward = np.sum(self.map >= 1) - 0.01* np.sum(self.map)
            #     done = True
            else:
                #self.map[self.position] += 1
                # if(np.sum(self.map >= 1) >= 90):
                #     print(self.map)
                #     input()
                #     reward = np.sum(self.map >= 1) - 0.1 * np.sum(self.map) 

                #     done = True
                # else:
                #     reward = np.sum(self.map >= 1) - 0.1 * np.sum(self.map) 
                #     done = False
                if(self.map[self.position[0],self.position[1]] == 0):
                    reward = 0 
                    done = False
                else:
                    reward = -1
                    done = False
                if(np.sum(self.map[:-1,:] >= 1) >= self.h * self.l - 4):
                    reward = 100
                    done = True
                    print("done")
                elif(np.sum(self.map) >= 2 * self.h * self.l):
                    reward = np.sum(self.map >= 1) - 0.01* np.sum(self.map)
                    done = True
                    print("done")
                else:
                    done = False
                
                
                
                self.map[self.position[0],self.position[1]] += 1
            if done:    
                print("reward",reward)
                rewa.append(reward)
                
                
        if goal == "target":
            print("target", self.step_c)
            if (self.step_c >= 40):
                done = True
                reward = 0
                self.step_c = 0
            else:
                if(self.map[self.position[0],self.position[1]] == -1 or cross):
                    reward = -1
                    done = True
                    print("done")
                else:
                    if (self.position[0] == 3 and self.position[1] == 3):
                        reward = 1
                        done = True
                    else:
                        reward = 0
                        done = False
                    #self.map[self.position[0],self.position[1]] += 1
            if done:    
                #print("reward",reward)
                rewa.append(reward)
        
        
        
        self.map[self.h,0] = self.position[0]
        self.map[self.h,1] = self.position[1]
        print("pos",self.map)
        s_ = self.map
        
        #print("s",s_, reward, done, self.position)
        return s_, reward, done, self.position
    
    def plt(self):
        plt.title("reward")
        plt.xlabel("times")
        plt.ylabel("reward")
        x = []
        for i in range(len(rewa)):
            x.append(i)
        plt.plot(x, rewa, label = "reward")
        plt.show()