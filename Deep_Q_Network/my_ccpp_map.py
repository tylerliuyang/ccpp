import numpy as np
import matplotlib.pyplot as plt

rewa = []

#goal = "ccpp"
goal = "target"

class envi():
    '''class envi:the environment
    env_size 11*10
    action_size 3
    '''
    def __init__(self) -> None:
        self.h = 4
        self.l = 4
        if goal == "ccpp":
            self.env_size = self.h * self.l + 2
        if goal == "target":
            self.env_size = 2
        self.action_size = 4
        
        self.map = np.zeros((self.h + 1, self.l))
        self.map[1,1] = -1
        self.obs = 1
        #self.map[4,4] = -1
        #self.position = [0,0]
        self.map[0,0] = 1
        self.step_c = 0
        self.goal_point = [3,3]
       
        
        
        
        
        

    def reset(self):
        '''[summary] reset the maze 
        Returns:
            [self.map]: 11*10 matrix
        '''
        self.__init__()
        
        #return self.map
        if goal == "ccpp":
            return self.map.flatten()[:self.env_size]
        if goal == "target":
            return self.goal_point
    
    def step(self, action):
        '''[summary] 

        Args:
            action (one of [0,1,2,3]): [0 is up;1 is down;2 is left; 3 is right]

        Returns:
            [s_, reward, done, self.position]: [description]
        '''
        s = self.map
        self.position = self.map[self.h , 0:2].astype(int)
        self.step_c += 1
        cross = False
        if(action == 0):  #up
            if(self.position[0] >= 1):
                print("0 up")
                self.position[0] -= 1
            else:
                #self.position[0] -= 1
                cross = True

        if(action == 1):  #down
            if(self.position[0] < (self.h - 1)):
                print("1 down")
                self.position[0] += 1  
            else:
                #self.position[0] += 1
                cross = True
        
        if(action == 2):  #left
            if(self.position[1] >= 1):
                print("2 left")
                self.position[1] -= 1
            else:
                #self.position[1] -= 1
                cross = True
           
        if(action == 3):  #right
            if(self.position[1] < self.l - 1):
                print("3 right")
                self.position[1] += 1
            else:
                #self.position[1] += 1
                cross = True
        
        if goal == "ccpp":
        #hit the obstacle
        
            if(self.map[self.position[0],self.position[1]] == -1 or cross):
                reward = -10
                self.step_c = 0
                done = True
                print("hit the obstacle")
            # elif(np.sum(self.map) >= 400):
            #     reward = np.sum(self.map >= 1) - 0.01* np.sum(self.map)
            #     done = True
            else:
                if(self.map[self.position[0],self.position[1]] == 0):
                    reward = 1 
                    done = False
                else:
                    reward = -1
                    done = False
                if(np.sum(self.map[:-1,:] >= 1) >= self.h * self.l - 2):
                    reward = 2
                    done = True
                    self.step_c = 0
                    print("succeed")
                elif(np.sum(self.step_c) >= 3 * self.h * self.l):
                    reward = 0#np.sum(self.map >= 1) - 0.01* np.sum(self.map)
                    done = True
                    self.step_c = 0
                    print("done")
                else:
                    reward = 0
                    done = False
                
                
                
                self.map[self.position[0],self.position[1]] = 1
            if done:    
                print("reward",self.map)
                rewa.append(reward)
                
            self.map[self.h,0] = self.position[0]
            self.map[self.h,1] = self.position[1]
            #print("pos",self.map)
            s_ = self.map.flatten()[:self.env_size]
        
            #print("s",s_, reward, done, self.position)
            #return s_, reward, done, self.position
                
                
        if goal == "target":
            
            # if (self.step_c >= 40):
            #     done = True
            #     reward = -0.5
            #     self.step_c = 0
            # else:
            if(cross or self.map[self.position[0],self.position[1]] == -1):
                reward = -1
                done = True
                print("cross")
            else:
                if (self.position[0] == 3 and self.position[1] == 3):
                    reward = 1
                    done = True
                    print("succeed")
                else:
                    reward = 0
                    done = False
            # if self.step_c >= 20:
            #     reward = 0
            #     done = True
                    
                #self.map[self.position[0],self.position[1]] += 1
                
            # self.map[self.h,0] = self.position[0]
            # self.map[self.h,1] = self.position[1]
            # print("pod\s",self.position)
                
            if done: 
                print("target", self.step_c)
                self.step_c = 0   
                print("reward",self.map,reward)
                rewa.append(reward)
        
        
        
            self.map[self.h,0] = self.position[0]
            self.map[self.h,1] = self.position[1]
            #print("pos",self.map)
            s_ = [3 - self.position[0], 3 - self.position[1]]
        
        #print("s",s_, reward, done, self.position)
        return s_, reward, done, self.position
    
    def plt(self):
        plt.title("reward")
        plt.xlabel("times")
        plt.ylabel("reward")
        x = []
        for i in range(len(rewa)):
            x.append(i)
        plt.plot(x, rewa, label = "reward")
        plt.show()